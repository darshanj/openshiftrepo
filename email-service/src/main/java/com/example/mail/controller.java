package com.example.mail;

import javax.mail.MessagingException; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class controller {


    @Autowired
    private EmailService emailService;
    
    @RequestMapping("/sendmail/{tomail}")
    public String sendmail(@PathVariable String tomail) throws MessagingException {

        Mail mail = new Mail();
        mail.setFrom("darshanj@wavelabs.in");
        mail.setTo(tomail);
        mail.setSubject("Sending Email with Attachment USing My Email Service");
        mail.setContent("EMail with Attachement using spring boot application");

        emailService.sendSimpleMessage(mail);
        
        return " email send to "+tomail+" with attachement ";
    }
}
